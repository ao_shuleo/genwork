#ifndef LINKEDGRAPH_H
#define LINKEDGRAPH_H

#include <QObject>
#include <QVariant>

class LinkedGraph;

class LinkedElement : public QObject{
    LinkedGraph *mGraph {nullptr};
    QList<LinkedElement*> mParentElements;

    int mLevel {0};
    int mId {-1};
    QList<LinkedElement*> mChildElements;

    QVariant mValue;

public:
    LinkedElement(int id, LinkedElement *parentElement, LinkedGraph*parentGraph);

    QVariant getValue() const { return mValue; }
    int getLevel() const { return mLevel;}
    QList<LinkedElement*> getParentElements() const { return mParentElements; }

    void setValue(const QVariant &val) { mValue = val; }
    void setLevel(int level) { mLevel = level; }

    LinkedElement *addChildElement(const QVariant &val);

    void connectChildElement(LinkedElement *child);
    void connectParent(LinkedElement *parent);

    bool hasChild() const;

    int id() const { return mId; }

    void rootChildDetour(const std::function<bool(LinkedElement*)> &callback);
    void childRootDetour(const std::function<bool(LinkedElement*)> &callback);

    void clear();

    QString toJSONTree() const;
    QString toJSONElement() const;
};

class LinkedGraph : public QObject{
    LinkedElement *mRootElement {nullptr};
    QMap<int, LinkedElement*> mElHash;

public:
    LinkedGraph(QObject *parent = nullptr);

    int getCountByLevel(int level);
    QList<LinkedElement*> getElementsByLevel(int level);
    int getLevelCount();

    LinkedElement *setRootElement(const QVariant &val);
    LinkedElement *addChildElement(const QVariant &val);

    bool contains(const QVariant &value, int level = -1);


    void rootChildDetour(const std::function<bool(LinkedElement*)> &callback);
    void childRootDetour(const std::function<bool(LinkedElement*)> &callback);
    void foreachDetour(const std::function<bool(LinkedElement*)> &callback);


    /*! Обход в порядке: сначала без потомков, слева направо, сверху вниз
        затем только с потомками, слева направо, снизу вверх

        Возвр. список элементов и границу, которая отделяет без/с потомками
     */
    std::pair<QList<LinkedElement *>, int> espDetour();

    QImage drawGraph(int width = 962, int height = 416);

    void clear();

    QString toJSONTree() const;
    QString toJSONGraph() const;

    int getUniqueId() const;

private:
    void registerElement(LinkedElement *el);

friend class LinkedElement;
};

#endif // LINKEDGRAPH_H
