#include "linkedgraph.h"

#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QGraphicsView>

LinkedGraph::LinkedGraph(QObject *parent) : QObject(parent)
{

}

int LinkedGraph::getCountByLevel(int level){
    int count = 0;
    for(const auto &el : qAsConst(mElHash)){
        if(el->getLevel() == level)
            count++;
    }

    return count;
}

QList<LinkedElement *> LinkedGraph::getElementsByLevel(int level)
{
    QList<LinkedElement *> resultList;
    for(const auto &el : qAsConst(mElHash)){
        if(el->getLevel() == level)
            resultList.append(el);
    }

    return resultList;
}

int LinkedGraph::getLevelCount()
{
    int maxLevel = 0;
    rootChildDetour([&](auto *el){
        if(el->getLevel() > maxLevel)
            maxLevel = el->getLevel();
        return true;
    });
    return maxLevel;

}

LinkedElement *LinkedGraph::setRootElement(const QVariant &val)
{
    auto el = new LinkedElement(0, nullptr, this);
    el->setValue(val);
    el->setLevel(0);
    if(mRootElement){
        mElHash.remove(mRootElement->id());
        mRootElement->deleteLater();
    }
    mRootElement = el;
    mElHash.insert(0, el);
    return el;
}

LinkedElement *LinkedGraph::addChildElement(const QVariant &val)
{
    if(!mRootElement){
        setRootElement({});
    }
    return mRootElement->addChildElement(val);
}

bool LinkedGraph::contains(const QVariant &value, int level)
{
    for(const auto &el : qAsConst(mElHash)){
        if(level < 0 || el->getLevel() == level){
            if(el->getValue() == value){
                return true;
            }
        }
    }
    return false;
}

void LinkedGraph::rootChildDetour(const std::function<bool (LinkedElement *)> &callback)
{
    if(mRootElement)
        mRootElement->rootChildDetour(callback);
}

void LinkedGraph::childRootDetour(const std::function<bool (LinkedElement *)> &callback)
{
    if(mRootElement)
        mRootElement->childRootDetour(callback);
}

void LinkedGraph::foreachDetour(const std::function<bool (LinkedElement *)> &callback)
{
    for(const auto &el : qAsConst(mElHash)){
        if(!callback(el))
            break;
    }
}

std::pair<QList<LinkedElement *>, int> LinkedGraph::espDetour()
{
    QList<LinkedElement *> resultList;
    rootChildDetour([&](auto *el){
        if(!el->hasChild())
            resultList.append(el);
        return true;
    });
    int sepIndex = resultList.size();

    childRootDetour([&](auto *el){
        if(el->hasChild())
            resultList.append(el);
        return true;
    });

    return std::pair<QList<LinkedElement *>, int>{resultList, sepIndex};
}



QPoint getCenter(QGraphicsItem *el){
    const auto r = el->boundingRect();
    return {(int)r.center().x(), (int)r.center().y()};
}

QImage LinkedGraph::drawGraph(int width, int height)
{
    QGraphicsScene *scene = new QGraphicsScene;
    const auto MWidth = width;
    const auto MHeight = height;
    const auto CDiag= 45;
    const auto RDiag = 6;
    const auto FontPointSize = 12;

    auto maxLevelElCount = 0;
    const auto lCount = getLevelCount();
    for(int i = 0; i < lCount; ++i){
        const auto levelCount = getCountByLevel(i);
        if(maxLevelElCount < levelCount)
            maxLevelElCount = levelCount;

    }
    scene->setSceneRect(0, 0, MWidth, MHeight);

    const auto stepY = MHeight / (lCount + 1);

    QPen cPen;
    cPen.setWidth(2);
    QFont cFont;
    cFont.setBold(true);
    cFont.setPointSize(FontPointSize);

//    QGraphicsItem *rootC;
    QHash<LinkedElement*, QPoint> mLowPointMap;

//    if(mRootElement && mRootElement->getValue().isValid()){
//        const auto cElX =  MWidth / 2 - CDiag/2;
//        const auto cElY = 0;
//        rootC = scene->addEllipse(cElX, cElY, CDiag, CDiag, cPen, QBrush(Qt::white));;
//        const auto t = scene->addSimpleText(mRootElement->getValue().toString(), cFont);
//        t->setPos(cElX + (CDiag - t->boundingRect().width())/2,
//                  cElY + (CDiag - t->boundingRect().height())/2);
//        mLowPointMap[mRootElement] = getCenter(rootC);
//    }
//    else{
//        rootC = scene->addEllipse((MWidth - RDiag)/2, stepY/4, RDiag, RDiag, cPen, QBrush(Qt::black)); // root element
//        mLowPointMap[mRootElement] = getCenter(rootC);
//    }

    int elHeight = CDiag;
    int elWidth = CDiag;
    auto elColor = Qt::white;
    QGraphicsItem *t {nullptr};

    for(int i = 0; i <= lCount; ++i){
        const auto levelEls = getElementsByLevel(i);
        const auto elCount = levelEls.size();

        const auto stepX = MWidth / (elCount + 1);

        for(int j = 0; j < elCount; ++j){
            const auto el = levelEls[j];
            auto cElY = stepY * i + 1;

            elHeight = CDiag;
            elColor = Qt::white;
            t = nullptr;

            if(!el->getValue().isValid()){
                elWidth = RDiag;
                elHeight = RDiag;
                elColor = Qt::black;
                cElY += RDiag;
            }
            else{
                t = scene->addSimpleText(el->getValue().toString(), cFont);
                t->setZValue(10);
                elWidth = CDiag*0.4 + t->boundingRect().width();
                if(elWidth < CDiag)
                    elWidth = CDiag;
            }

            const auto cElX = stepX * (j+1) - elWidth/2;
            const auto cEl = scene->addEllipse(cElX, cElY, elWidth, elHeight, cPen, QBrush(elColor));

            if(t)
                t->setPos(cElX + (elWidth - t->boundingRect().width())/2,
                          cElY + (CDiag - t->boundingRect().height())/2);

            const auto p2 = getCenter(cEl);
            mLowPointMap[el] = p2;

            const auto parentEl = el->getParentElements();
            for(const auto &el : qAsConst(parentEl)){
                const auto p1 = mLowPointMap[el];
                const auto l = scene->addLine(p1.x(), p1.y(), p2.x(), p2.y(), cPen);
                l->setZValue(-100);
            }
        }
    }

//    QGraphicsView *view = new QGraphicsView(scene);
//    view->setRenderHint(QPainter::Antialiasing);
//    view->show();

    QImage image(scene->sceneRect().size().toSize(), QImage::Format_ARGB32);
    image.fill(Qt::transparent);

    QPainter painter(&image);
    painter.setRenderHint(QPainter::Antialiasing);
    scene->render(&painter);

    return image;
}

void LinkedGraph::clear()
{
    if(mRootElement){
        mRootElement->clear();
        mRootElement->deleteLater();
        mRootElement = nullptr;
    }
    mElHash.clear();
}

QString LinkedGraph::toJSONTree() const
{
    if(mRootElement){
        return mRootElement->toJSONTree();
    }
    return "{}";
}

QString LinkedGraph::toJSONGraph() const
{
    QString str = "{\"els\":[";
    int i = 0;
    for(const auto &el : qAsConst(mElHash)){
        if(i != 0){
            str += ",";
        }
        str += el->toJSONElement();
    }
    str += "]}";
    return str;
}

LinkedElement::LinkedElement(int id, LinkedElement *parentElement,
                             LinkedGraph *parentGraph) :
    QObject(parentGraph),
    mGraph(parentGraph),
    mId(id)
{
    if(parentElement)
        mParentElements.append(parentElement);
}

LinkedElement *LinkedElement::addChildElement(const QVariant &val)
{
    auto el = new LinkedElement(mGraph->getUniqueId(), this, mGraph);
    el->setValue(val);
    el->setLevel(mLevel+1);

    mChildElements.append(el);
    mGraph->registerElement(el);
    return el;
}

void LinkedElement::connectChildElement(LinkedElement *child)
{
    if(!mChildElements.contains(child)){
        mChildElements.append(child);
    }
}

void LinkedElement::connectParent(LinkedElement *parent)
{
    if(!mParentElements.contains(parent)){
        mParentElements.append(parent);
        parent->connectChildElement(this);
    }
}

bool LinkedElement::hasChild() const
{
    return mChildElements.size() > 0;
}

void LinkedElement::rootChildDetour(const std::function<bool (LinkedElement *)> &callback)
{
    if(callback(this))
        for(const auto &el : qAsConst(mChildElements)){
            el->rootChildDetour(callback);
        }
}

void LinkedElement::childRootDetour(const std::function<bool (LinkedElement *)> &callback)
{
    for(const auto &el : qAsConst(mChildElements)){
        el->childRootDetour(callback);
    }
    callback(this);
}

void LinkedElement::clear()
{
    for(const auto &el : qAsConst(mChildElements)){
        el->clear();
        el->deleteLater();
    }
    mChildElements.clear();
}

QString LinkedElement::toJSONTree() const
{
    QString str("{\"v\":\"%1\",\"l\":%2,\"ch\":[%3],\"id\":%4}");
    QString chStr;
    for(int i = 0; i < mChildElements.size(); ++i){
        if(i != 0)
            chStr +=",";
        chStr += mChildElements[i]->toJSONTree();
    }
    return str.arg(mValue.toString()).arg(mLevel).arg(chStr).arg(mId);
}

QString LinkedElement::toJSONElement() const
{
    QString str("{\"v\":\"%1\",\"l\":%2,\"ch\":[%3],\"pr\":[%4],\"id\":%5}");
    QString chStr, prStr;
    for(int i = 0; i < mChildElements.size(); ++i){
        if(i != 0)
            chStr +=",";
        chStr += QString::number(mChildElements[i]->id());
    }
    for(int i = 0; i < mParentElements.size(); ++i){
        if(i != 0)
            prStr +=",";
        prStr += QString::number(mParentElements[i]->id());
    }

    return str.arg(mValue.toString()).arg(mLevel).arg(chStr, prStr).arg(mId);
}

int LinkedGraph::getUniqueId() const
{
    int id = 1;
    for(const auto &el : qAsConst(mElHash)){
        if(el->id() != 0){
            if(id == el->id()){
                id++;
            }
            else
                return id;
        }
    }
    return id;
}

void LinkedGraph::registerElement(LinkedElement *el)
{
    mElHash[el->id()] = el;
}
