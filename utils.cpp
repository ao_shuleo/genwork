#include "utils.h"

#include "qcustomplot.h"
#include <QImage>

QImage drawPoints(const QList<QPointF> &mPoints, int maxX, int maxY, int width, int height){
    QCustomPlot plot;
    auto g = plot.addGraph();

    int i = 0;
    QFont font;
    font.setItalic(true);
    for(const auto &el : qAsConst(mPoints)){
        g->addData(el.x(), el.y());

        QCPItemText *textLabel = new QCPItemText(&plot);
        textLabel->setPositionAlignment(Qt::AlignBottom|Qt::AlignLeft);
        textLabel->position->setType(QCPItemPosition::ptPlotCoords);
        textLabel->position->setCoords(el.x()+0.1, el.y()+0.1); // place position at center/top of axis rect
        textLabel->setText(QString::number(i));
        textLabel->setFont(font);

        ++i;
    }
    g->setLineStyle(QCPGraph::lsNone);
    g->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 10));

    plot.xAxis->setRange(0, maxX);
    plot.yAxis->setRange(0, maxY);

    QSharedPointer<QCPAxisTickerFixed> f(new QCPAxisTickerFixed);
    plot.xAxis->setTicker(f);
    plot.yAxis->setTicker(f);
    f->setTickStep(1.0);

    plot.xAxis->setSubTicks(false);
    plot.yAxis->setSubTicks(false);



    const auto pix = plot.toPixmap(width, height);
    return pix.toImage();
}

double NormalPDF(double x, double m, double s)
{
    static const double sqrt_2pi = 2.50662827463;
    return std::exp(-0.5f * std::pow((x - m) / s, 2.)) / (sqrt_2pi*s);
}

double NormalCDF(double x, double m, double s)
{
    static const double sqrt_2pi = 2.50662827463;
    double r = 0;
    double it = m - 4*s;
    while(it < x){
        r += std::exp(-0.5f * std::pow((it - m) / s, 2.)) / (sqrt_2pi*s);
        it += 0.001;
    }
    return r;
}

QString ConvertDoubleToStringWithAccuracy(const double val, const int accuracy){
    const auto result {QString::number(val, 'f', accuracy)};
    if(accuracy <= 0)
        return result;
    if(result.contains('.') || result.contains(',')){
        int index = result.length();
        while(result.at(--index) == '0');
        if(result.at(index) == '.' || result.at(index) == ',')
            return result.left(index);
        else
            return result.left(index+1);
    }
    return result;
}

std::pair<float, float> genRadPoint(float x, float y, float rMin, float rMax, QRandomGenerator &rgen){
    const float rAngle = rgen.bounded(2*M_PI);
    const float rRadius = (rMax-rMin)*rgen.generateDouble() + rMin;
    return {x + std::cos(rAngle) * rRadius, y + std::sin(rAngle) * rRadius};
}

float genRangePoint(float x, float rMin, float rMax, QRandomGenerator &rgen){
    const float rRadius = (rgen.generateDouble() < 0.5 ? -1 : 1) * ((rMax-rMin)*rgen.generateDouble() + rMin);
    return x + rRadius;
}


QStringList _GenFullSet(int level, const QList<char> &I, const QString &prefix){
    QStringList levelResult;
    int start = 0;
    if(prefix.length() == 0)
        start = 0;
    else
        start = I.indexOf(prefix[prefix.length()-1].toLatin1())+1;
    for(int i = start; i < I.length(); ++i){
        const auto el = prefix + I[i];
        if(level != 0)
            levelResult.append(_GenFullSet(level-1, I, el));
        else
            levelResult.append(el);
    }
    return levelResult;
}

QStringList GenFullSet(const QList<char> &I){
    QStringList result;
    for(int i = 0; i < I.size(); ++i){
        result.append(_GenFullSet(i, I, ""));
    }
    return result;
}

bool r_IsSubSequence(const QString &str1, const QString &str2, int m, int n){
    if(m == 0) return true;
    if(n == 0) return false;
    if(str1[m-1]==str2[n-1])
        return r_IsSubSequence(str1, str2, m-1, n-1);
    return r_IsSubSequence(str1, str2, m, n-1);
}

bool IsSubSequence(const QString &str1, const QString &str2){
    return r_IsSubSequence(str1, str2, str1.length(), str2.length());
}

int GetFreqSubstringInSet(const QString &sub, const QStringList &dataSet){
    int result = 0;
    for(const auto &el : qAsConst(dataSet)){
        if(IsSubSequence(sub, el))
            result++;
    }
    return result;
}
