#ifndef WORKGENERATOR_H
#define WORKGENERATOR_H

#include <QObject>
#include <QSettings>

struct GenReturnType{
    QStringList mainData;
    QStringList addData;
    QHash<QString, QImage> images;
};

class WorkGenerator : public QObject
{
    Q_OBJECT

    QString mLastError;

    QSettings *mSettings;

public:
    explicit WorkGenerator(QObject *parent = nullptr);

    QString mTemplatePath;
    QString mSaveDir;
    QString mFileNameTemplate;
    int mWorkNumber {-1};
    int mVarCount {1};

    int mGenSeed {42};
    bool mIsRandomSeed {true};

    bool addCopyFiles {false};
    QString mCopyFilesPath;

    bool mCreateDirForVar {false};

    bool setTemplatePath(const QString &filePath);
    bool setSavePath(const QString &saveDirPath);
    bool setWorkNumber(int num);
    void setVariantCount(int count);
    void setFileNameTemplate(const QString &name);
    void setRandomGen(bool enable);
    void setGenerationSeed(int seed);
    void setAddCopyFiles(bool enable);
    void setCopyFilesPath(const QString &path);

    void setCreateDirForVar(bool enable);

    bool generate(bool genExample = false);

    QList<std::pair<QString, int>> getAvailableWorks() const;

    QString getLastError() const;

    void readSettings();

signals:

private:
    GenReturnType genWork_1(int currentVariant);
    GenReturnType genWork_2(int currentVariant);
    GenReturnType genWork_3(int currentVariant);
    GenReturnType genWork_4(int currentVariant);
    GenReturnType genWork_5(int currentVariant);
    GenReturnType genWork_6(int currentVariant);

    GenReturnType genWork_3_4(int currentVariant);

    GenReturnType genExampleWork_4(int);
    GenReturnType genExampleWork_5(int);
    GenReturnType genExampleWork_6(int);

    GenReturnType genExampleWork_3_4(int);
};

#endif // WORKGENERATOR_H
