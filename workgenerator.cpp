#include "workgenerator.h"

#include <QFile>
#include <QDir>
#include <QRandomGenerator>

#include <QDateTime>
#include <QTextStream>


#include <QDebug>
#include <QImage>

#include <random>

#include "linkedgraph.h"
#include "qcustomplot.h"
#include "mrange.h"

#include "utils.h"

WorkGenerator::WorkGenerator(QObject *parent) : QObject(parent)
{
    mSettings = new QSettings("app_settings", QSettings::IniFormat, this);
}

bool WorkGenerator::setTemplatePath(const QString &filePath)
{
    mTemplatePath = filePath;
    mSettings->setValue("mTemplatePath", mTemplatePath);
    return true;
}

bool WorkGenerator::setSavePath(const QString &saveDirPath)
{
    mSaveDir = saveDirPath;
    mSettings->setValue("mSaveDir", mSaveDir);
    return true;
}

bool WorkGenerator::setWorkNumber(int num)
{
    mWorkNumber = num;
    mSettings->setValue("mWorkNumber", mWorkNumber);
    return true;
}

void WorkGenerator::setVariantCount(int count)
{
    mVarCount = count;
    mSettings->setValue("mVarCount", mVarCount);
}

void WorkGenerator::setFileNameTemplate(const QString &name)
{
    mFileNameTemplate = name;
    mSettings->setValue("mFileNameTemplate", mFileNameTemplate);
}

void WorkGenerator::setRandomGen(bool enable)
{
    mIsRandomSeed = enable;
    mSettings->setValue("mIsRandomSeed", enable);
}

void WorkGenerator::setGenerationSeed(int seed)
{
    mGenSeed = seed;
    mSettings->setValue("mGenSeed", seed);
}

void WorkGenerator::setAddCopyFiles(bool enable)
{
    addCopyFiles = enable;
    mSettings->setValue("addCopyFiles", enable);
}

void WorkGenerator::setCopyFilesPath(const QString &path)
{
    mCopyFilesPath = path;
    mSettings->setValue("mCopyFilesPath", path);
}

void WorkGenerator::setCreateDirForVar(bool enable)
{
    mCreateDirForVar = enable;
    mSettings->setValue("mCreateDirForVar", enable);
}

bool WorkGenerator::generate(bool genExample)
{
    QFile temlFile(mTemplatePath);
    if(!temlFile.open(QIODevice::ReadOnly)){
        mLastError = tr("не могу открыть файл шаблона");
        return false;
    }

    const QString temlFileStr = QString::fromUtf8(temlFile.readAll());
    if(temlFileStr.isEmpty()){
        mLastError = tr("файл шаблона пуст");
        return false;
    }

    std::function<GenReturnType(int)> mGenFunction;

    if(!genExample){
        switch (mWorkNumber) {
        case 0:
            mGenFunction = [this](int curV){ return genWork_1(curV);};
            break;
        case 1:
            mGenFunction = [this](int curV){ return genWork_2(curV);};
            break;
        case 2:
            mGenFunction = [this](int curV){ return genWork_3(curV);};
            break;
        case 3:
            mGenFunction = [this](int curV){ return genWork_4(curV);};
            break;
        case 4:
            mGenFunction = [this](int curV){ return genWork_5(curV);};
            break;
        case 5:
            mGenFunction = [this](int curV){ return genWork_6(curV);};
            break;
        case 6:
            mGenFunction = [this](int curV){ return genWork_3_4(curV);};
            break;
        default:{
            mLastError = tr("данное задание не поддерживается");
            return false;
        }
        }
    }
    else{
        switch (mWorkNumber) {
        case 3:
            mGenFunction = [this](int curV){ return genExampleWork_4(curV);};
            break;
        case 4:
            mGenFunction = [this](int curV){ return genExampleWork_5(curV);};
            break;
        case 5:
            mGenFunction = [this](int curV){ return genExampleWork_6(curV);};
            break;
        case 6:
            mGenFunction = [this](int curV){ return genExampleWork_3_4(curV);};
            break;
        default:{
            mLastError = tr("данное задание не поддерживается");
            return false;
        }
        }
    }

    for(int i = 0; i < mVarCount; ++i){
        if(genExample)
            i = -1;

        auto curDataStr = temlFileStr;
        const auto genResult = mGenFunction(i);

        for (int j = 0; j < genResult.mainData.size(); ++j) {
            curDataStr = curDataStr.arg(genResult.mainData.at(j));
        }

        QString nFileName = mFileNameTemplate;
        nFileName.replace("{%n}", QString::number(mWorkNumber + 1));
        nFileName.replace("{%v}",   QString::number(i + 1));
        QString mAddDir = "";
        if(mCreateDirForVar){
            QFileInfo fInfo(nFileName);
            mAddDir = fInfo.baseName() + "/";
            QDir d;
            if(!d.exists(mSaveDir + "/" + mAddDir))
                d.mkpath(mSaveDir + "/" + mAddDir);

            if(addCopyFiles){
                QDir d(mCopyFilesPath);
                const auto fList = d.entryList(QDir::Files);
                for(const auto &el : qAsConst(fList)){
                    QFileInfo fInfo(el);
                    QFile::copy(mCopyFilesPath + "/" + el, mSaveDir + "/" + mAddDir + fInfo.fileName());
                }
            }

        }

        QFile nFile(mSaveDir + "/" + mAddDir + nFileName);
        if(!nFile.open(QIODevice::WriteOnly)){
            mLastError = tr("не могу создать файл %1").arg(nFileName);
            return false;
        }

        nFile.write(curDataStr.toUtf8());

        const auto addDataFileName = QString("addData%1_%2.txt").arg(mWorkNumber + 1).arg(i + 1);
        QFile nAddDataFile(mSaveDir + "/" + addDataFileName);
        if(!nAddDataFile.open(QIODevice::WriteOnly)){
            mLastError = tr("не могу создать файл доп. данных %1").arg(addDataFileName);
            return false;
        }

        QTextStream addDataStream(&nAddDataFile);
        for(const auto &str : qAsConst(genResult.addData)){
            addDataStream << str << "\r\n";
        }
        addDataStream << ";;"<< "\r\n";
        for(const auto &str : qAsConst(genResult.mainData)){
            addDataStream << str << "\r\n";
        }


        auto imIter = genResult.images.constKeyValueBegin();
        while(imIter != genResult.images.constKeyValueEnd()){
            imIter->second.save(mSaveDir + "/" + mAddDir + imIter->first);
            imIter++;
        }

        if(genExample)
            break;

    }

    if(addCopyFiles && !mCreateDirForVar){
        QDir d(mCopyFilesPath);
        const auto fList = d.entryList(QDir::Files);
        for(const auto &el : qAsConst(fList)){
            QFileInfo fInfo(el);
            QFile::copy(mCopyFilesPath + "/" + el, mSaveDir + "/" + fInfo.fileName());
        }
    }

    mLastError = "";

    return true;
}

QList<std::pair<QString, int> > WorkGenerator::getAvailableWorks() const
{
    return {{tr("Практическая работа №1"), 0},
            {tr("Практическая работа №2"), 1},
            {tr("Практическая работа №3"), 2},
            {tr("Практическая работа №4"), 3},
            {tr("Практическая работа №5"), 4},
            {tr("Практическая работа №6"), 5},
            {tr("Практическая работа №3-4"), 6},
    };
}

QString WorkGenerator::getLastError() const
{
    return mLastError;
}

void WorkGenerator::readSettings()
{
    mTemplatePath = mSettings->value("mTemplatePath").toString();
    mSaveDir = mSettings->value("mSaveDir").toString();
    mFileNameTemplate = mSettings->value("mFileNameTemplate", tr("Практическое задание №%1_%2.md")).toString();
    mWorkNumber = mSettings->value("mWorkNumber").toInt();
    mVarCount = mSettings->value("mVarCount").toInt();
    mGenSeed = mSettings->value("mGenSeed", 42).toInt();
    mIsRandomSeed = mSettings->value("mIsRandomSeed", true).toBool();
    addCopyFiles = mSettings->value("addCopyFiles", false).toBool();
    mCopyFilesPath = mSettings->value("mCopyFilesPath").toString();
    mCreateDirForVar = mSettings->value("mCreateDirForVar", false).toBool();
}

GenReturnType WorkGenerator::genWork_1(int currentVariant)
{
    QStringList addData;

    const auto rSeed = mGenSeed + currentVariant + 1;

    std::random_device rd{};
    std::default_random_engine generator;
    if(mIsRandomSeed){
        generator.seed(rd());
    }
    else{
        generator.seed(rSeed);
        std::srand(rSeed);
    }
    QRandomGenerator rgen(mIsRandomSeed ?
                          (QDateTime::currentMSecsSinceEpoch() / (currentVariant + 1)) :
                          rSeed);

    // задание 1
    int mA = rgen.bounded(50, 80);
    int skoA = rgen.bounded(1, 40);
    int mL = rgen.bounded(160, 180);
    int skoL = rgen.bounded(10, 50);

    int thA = rgen.bounded(40, 70);

    addData.append(QString("%1, %2").arg(mA).arg(skoA));
    addData.append(QString("%1, %2").arg(mL).arg(skoL));

    std::normal_distribution<> nNormalA(mA, skoA);
    std::normal_distribution<> nNormalL(mL, skoL);

    const int vSize = 20;
    QString vX;
    QString vY;

    for(int i = 0; i < vSize; ++i){
        if(i != 0){
            vX += ", ";
            vY += ", ";
        }
        vX += QString::number((int)nNormalA(generator));
        vY += QString::number((int)nNormalL(generator));
    }

    // задание 2
    int mR = rgen.bounded(45, 55);
    std::normal_distribution<> nNormalR(mR, skoL);

    addData.append(QString("%1, %2").arg(mR).arg(skoL));

    QList<int> aR = {rgen.bounded(0, 50), rgen.bounded(0, 50), rgen.bounded(0, 50)};
    QList<int> bR = {(int)nNormalA(generator), (int)nNormalA(generator), (int)nNormalA(generator)};
    QList<int> cR = {rgen.bounded(0, 5), rgen.bounded(0, 5), rgen.bounded(0, 5)};

    // задание 3
    int m1 = rgen.bounded(2, 12), m2 = 0;
    int sko1 = rgen.bounded(1, 6), sko2 = 1;

    do{
        m2 = rgen.bounded(2, 12);
    }while(m1 == m2);
    do{
        sko2 = rgen.bounded(1, 6);
    }while(sko1 == sko2);

    int vCount = rgen.bounded(2, 4);

    QList<int> vVal;
    for(int i = 0; i < vCount; ++i){
        int t;
        do{
            t = rgen.bounded(2, 12);
        }while(vVal.contains(t));

        vVal.append(t);
    }
    std::sort(vVal.begin(), vVal.end());

    QString vV;
    for(int i = 0; i < vCount; ++i){
        if(i != 0){
            vV += ", ";
        }
        vV += QString::number(vVal[i]);
    }

    return {{
        QString::number(currentVariant + 1),
        vX,
        vY,
        QString::number(thA),
        QString::number(aR[0]), QString::number(aR[1]), QString::number(aR[2]),
        QString::number(bR[0]), QString::number(bR[1]), QString::number(bR[2]),
        QString::number(cR[0]), QString::number(cR[1]), QString::number(cR[2]),
        QString("%1, %2").arg(m1).arg(m2),
        QString("%1, %2").arg(sko1).arg(sko2),
        vV
        }, addData, {}};
}

GenReturnType WorkGenerator::genWork_2(int currentVariant)
{
    QStringList templateData;
    QStringList addData;

    templateData.append(QString::number(currentVariant + 1));

    const auto rSeed = mGenSeed + currentVariant + 1;

    std::random_device rd{};
    std::default_random_engine generator;
    if(mIsRandomSeed){
        generator.seed(rd());
    }
    else{
        generator.seed(rSeed);
        std::srand(rSeed);
    }
    QRandomGenerator rgen(mIsRandomSeed ?
                          (QDateTime::currentMSecsSinceEpoch() / (currentVariant + 1)) :
                          rSeed);

    // задание 1
    int firstCenterA = rgen.bounded(-10, 10);
    int firstCenterB = rgen.bounded(-10, 10);
    auto secondCenter = genRadPoint(firstCenterA, firstCenterB, 5, 10, rgen);

    float radA = rgen.generateDouble() * 2 + 0.5;
    float radB = rgen.generateDouble() * 3 + 0.3;

    addData.append(QString("%1, %2, %3").arg(firstCenterA).arg(firstCenterB).arg(radA));
    addData.append(QString("%1, %2, %3").arg(secondCenter.first).arg(secondCenter.second).arg(radB));

    QList<std::pair<float, float>> points;
    for(int i = 0; i < 4; ++i){
        points.append(genRadPoint(firstCenterA, firstCenterB, 0, radA, rgen));
        points.append(genRadPoint(secondCenter.first, secondCenter.second, 0, radB, rgen));
    }
//    qDebug() << points;
    std::random_shuffle(points.begin(), points.end());
//    qDebug() << points;

    for(const auto &el : qAsConst(points)){
        templateData.append(ConvertDoubleToStringWithAccuracy(el.first, 1));
        templateData.append(ConvertDoubleToStringWithAccuracy(el.second, 1));
    }

    // задание 2
    int mX1 = rgen.bounded(-50, 10);
    int skoX1 = rgen.bounded(10, 100);
    int mX2 = rgen.bounded(-10, 90);
    int skoX2 = rgen.bounded(10, 100);

    std::normal_distribution<> nNormalX1(mX1, skoX1);
    std::normal_distribution<> nNormalX2(mX2, skoX2);

    addData.append(QString("%1, %2").arg(mX1).arg(skoX1));
    addData.append(QString("%1, %2").arg(mX2).arg(skoX2));

    for(int i = 0; i < 8; ++i){
        templateData.append(ConvertDoubleToStringWithAccuracy((int)nNormalX1(generator), 1));
        templateData.append(ConvertDoubleToStringWithAccuracy((int)nNormalX2(generator), 1));
    }

    return {templateData, addData, {}};
}

GenReturnType WorkGenerator::genWork_3(int currentVariant)
{
    QStringList templateData;
    QStringList addData;

    templateData.append(QString::number(currentVariant + 1));

    const auto rSeed = mGenSeed + currentVariant + 1;

    std::random_device rd{};
    std::default_random_engine generator;
    if(mIsRandomSeed){
        generator.seed(rd());
    }
    else{
        generator.seed(rSeed);
        std::srand(rSeed);
    }
    QRandomGenerator rgen(mIsRandomSeed ?
                          (QDateTime::currentMSecsSinceEpoch() / (currentVariant + 1)) :
                          rSeed);

    // задание 1
    const int MaxCountL = 7;
    const char StartL {'A'};
    for(int i = 0; i < 10; i++){
        QString str;
        do{
            str.clear();
            for(int j = 0; j < MaxCountL; ++j){
                if(rgen.generateDouble() < 0.45){
                    str += char((int)StartL + j);
                }
            }
        }
        while(str.length() <= 1);
        templateData.append(str);
    }

    const int apLevel0 = rgen.bounded(3, 6);
    int apLevel1 = 0;
    do{
        apLevel1 = rgen.bounded(3, 6);
    }
    while(apLevel1 == apLevel0);

    templateData.append(QString::number(apLevel0));
    templateData.append(QString::number(apLevel1));

    // задание 2
    const int aElCount[] = {0, 2, 3};
    auto g = new LinkedGraph;


    int generalElCount = 0;
    do{
        g->clear();
        generalElCount = 0;

        const auto l1Count = 4 /*rgen.bounded(3, 5)*/;
        generalElCount += l1Count;

        bool hasOneEl = false;

        for(int i = 0; i < l1Count; ++i){
            const auto n_el1 = g->addChildElement(0);
            auto l2Count = aElCount[rgen.bounded(0, 3)];

            if(l2Count == 0)
                hasOneEl = true;

            if(i == (l1Count-1) && !hasOneEl){
                l2Count = 0;
                hasOneEl = true;
            }

            generalElCount += l2Count;

            for(int j = 0; j < l2Count; ++j){
                const auto n_el2 = n_el1->addChildElement(0);
                const auto l3Count = aElCount[rgen.bounded(0, 3)];
                generalElCount += l3Count;

                for(int k = 0; k < l3Count; ++k){
                    n_el2->addChildElement(0);
                }
            }
        }
        auto gList = g->espDetour();

        char elN = 'A';
        for(const auto &el : qAsConst(gList.first)){
            el->setValue(QString(elN));
            elN++;
        }
    }
    while(generalElCount < 13 || generalElCount > 18);

    const auto img = g->drawGraph();
    QString imageName("n3_%1_1.png");
    imageName = imageName.arg(currentVariant + 1);

    QVector<QString> AllowedSet;
    const auto l = g->espDetour();

    for(int i = 0; i < l.second; ++i){
        AllowedSet.append(l.first[i]->getValue().toString());
    }

    addData.append(QString::number(l.second));
    QString addStr;
    for(int i = 0; i < l.first.size(); ++i){
        addStr += l.first[i]->getValue().toString();
    }
    addData.append(addStr);
    addData.append(g->toJSONTree());


    for(int i = 0; i < 10; i++){
        QString str;
        int sCount = 0;
        do{
            sCount = 0;
            str.clear();
            for(int j = 0; j < AllowedSet.length(); ++j){
                if(rgen.generateDouble() < 0.4){
//                    str += (str.length() > 0 ? ", " : "") + QString::number(AllowedSet[j]);
                    str += AllowedSet[j];
                    sCount++;
                }
            }
        }
        while(sCount <= 2);
        templateData.append(str);
    }

    const int apLevel3 = rgen.bounded(3, 8);
    templateData.append(QString::number(apLevel3));


    templateData.append(imageName);

    QHash<QString, QImage> images;
    images.insert(imageName, img);

    return {templateData, addData, images};
}



GenReturnType WorkGenerator::genWork_4(int currentVariant)
{
    GenReturnType result;

    result.mainData.append(QString::number(currentVariant + 1));

    const auto rSeed = mGenSeed + currentVariant + 1;

    std::random_device rd{};
    std::default_random_engine generator;
    if(mIsRandomSeed){
        generator.seed(rd());
    }
    else{
        generator.seed(rSeed);
        std::srand(rSeed);
    }
    QRandomGenerator rgen(mIsRandomSeed ?
                          (QDateTime::currentMSecsSinceEpoch() / (currentVariant + 1)) :
                          rSeed);

    // задание 1
    {

    const int MaxCountL = 5;
    const char StartL {'A'};
    for(int i = 0; i < 8; i++){
        QString str;
        do{
            str.clear();
            for(int j = 0; j < MaxCountL; ++j){
                if(rgen.generateDouble() < 0.6){
                    str += char((int)StartL + j);
                }
            }
        }
        while(str.length() <= 1);
        result.mainData.append(str);
    }

    const int apLevel0 = rgen.bounded(1, 6);
    result.mainData.append(QString::number(apLevel0));

    QString str;
    do{
        str.clear();
        for(int j = 0; j < MaxCountL; ++j){
            if(rgen.generateDouble() < 0.4){
                str += char((int)StartL + j);
            }
        }
    }
    while(str.length() == 0);
    result.mainData.append(str);

    }

    // задание 2
    {

    QList<char> ABCSymbolList = {'A', 'B', 'C', 'D', 'E'};
    const auto TidSetCount = 10;

    const auto fullABCSet = GenFullSet(ABCSymbolList);

    QStringList resultSet2;
    QString resultStr;
    for(int i = 0; i < TidSetCount; i++){
        QString str;
        do{
            str.clear();
            for(int j = 0; j < ABCSymbolList.size(); ++j){
                if(rgen.generateDouble() < 0.8){
                    str += ABCSymbolList[j];
                }
            }
//            qDebug() << str << resultSet2.contains(str);
        }
        while(str.length() <= 1 || resultSet2.contains(str));
        resultSet2.append(str);
        resultStr += str + ";";
    }

    result.addData.append(resultStr.chopped(1));
//    qDebug() << resultSet2;
//    qDebug() << fullABCSet;

    QVector<QStringList> listByLevels(ABCSymbolList.size());
    for(const auto &el : qAsConst(fullABCSet)){
        listByLevels[el.length()-1].append(el);
    }

//    qDebug() << listByLevels;

    LinkedGraph g;
    auto root = g.setRootElement("Ø");

    const static QString TreeValueTemplate("%1(%2)");

    for(int k = 0; k < listByLevels.size(); ++k){
        if(k == 0){
            for(const auto &el : qAsConst(listByLevels[k])){
                root->addChildElement(el);
            }
        }
        else{
            const auto k_1LevelElements = g.getElementsByLevel(k);
            for(const auto &el : qAsConst(listByLevels[k])){


                LinkedElement *currentElement = nullptr;
                for(const auto &listEl : k_1LevelElements){
                    if(el.startsWith(listEl->getValue().toString())){
                        currentElement = listEl->addChildElement(el);
                    }
                    else if(IsSubSequence(listEl->getValue().toString(), el)){
                        if(currentElement)
                            currentElement->connectParent(listEl);
                    }
                }

            }

        }
    }

    QString fqStr;
    g.foreachDetour([=, &fqStr](auto *el){
        if(el->getLevel() != 0){
            const auto v = el->getValue().toString();
            const auto fq = GetFreqSubstringInSet(v, resultSet2);
            el->setValue(TreeValueTemplate.arg(v).arg(fq));
            fqStr += QString("%1:%2;").arg(v).arg(fq);
        }
        return true;
    });
    result.addData.append(fqStr.chopped(1));


    const auto img = g.drawGraph(962, 481);
    QString imageName("n4_%1_1.png");
    imageName = imageName.arg(currentVariant + 1);
    result.mainData.append(imageName);
    result.images.insert(imageName, img);

    const int apLevel0 = rgen.bounded(2, 4);
    result.mainData.append(QString::number(apLevel0));

    const auto SuperStr = fullABCSet[rgen.bounded(0, fullABCSet.size())];
    result.mainData.append(SuperStr);
    } // 2

    // задание 3
    {

    const auto MaxCountSeq = 15;
    QList<char> SeqSymbolList = {'A', 'T', 'G', 'C'};

    for(int i = 0; i < 5; i++){
        QString str;
        do{
            str.clear();
            for(int j = 0; j < MaxCountSeq; ++j){
                if(rgen.generateDouble() < 0.8){
                    const auto sIndex = rgen.bounded(0, SeqSymbolList.length());
                    str += SeqSymbolList[sIndex];
                }
            }
        }
        while(str.length() <= 1);
        result.mainData.append(str);
    }
    const int apLevel1 = 4; /*rgen.bounded(3, 5);*/
    result.mainData.append(QString::number(apLevel1));

    QString str;
    do{
        str.clear();
        for(int j = 0; j < MaxCountSeq; ++j){
            if(rgen.generateDouble() < 0.5){
                const auto sIndex = rgen.bounded(0, SeqSymbolList.length());
                str += SeqSymbolList[sIndex];
            }
        }
    }
    while(str.length() <= 1);
    result.mainData.append(str);

    }

    return result;
}


std::pair<float, float> PointPC(float a, float u1, float u2, float s1, float s2, float pc1, float pc2){
    const auto pa1 = NormalPDF(a, u1, s1);
    const auto pa2 = NormalPDF(a, u2, s2);
    const auto pcaF = pa1*pc1+pa2*pc2;
    const auto pc1a = pa1*pc1/pcaF;
    const auto pc2a = pa2*pc2/pcaF;
    return {pc1a, pc2a};
}

GenReturnType WorkGenerator::genWork_5(int currentVariant)
{
    GenReturnType result;

    result.mainData.append(QString::number(currentVariant + 1));

    const auto rSeed = mGenSeed + currentVariant + 1;

    std::random_device rd{};
    std::default_random_engine generator;
    if(mIsRandomSeed){
        generator.seed(rd());
    }
    else{
        generator.seed(rSeed);
        std::srand(rSeed);
    }
    QRandomGenerator rgen(mIsRandomSeed ?
                          (QDateTime::currentMSecsSinceEpoch() / (currentVariant + 1)) :
                          rSeed);

    // задание 1
    {
        const int clusterCount = 2;

        int firstCenterA = rgen.bounded(-50, 50);
        int firstCenterB = rgen.bounded(-50, 50);
        auto secondCenter = genRadPoint(firstCenterA, firstCenterB, 15, 20, rgen);

        float radA = rgen.generateDouble() * 8 + 2;
        float radB = rgen.generateDouble() * 8 + 2;

        result.addData.append(QString("%1,%2,%3").arg(firstCenterA).arg(firstCenterB).arg(radA));
        result.addData.append(QString("%1,%2,%3").arg(secondCenter.first).arg(secondCenter.second).arg(radB));


        QList<std::pair<float, float>> points;
        for(int i = 0; i < 5; ++i){
            points.append(genRadPoint(firstCenterA, firstCenterB, 0, radA, rgen));
            points.append(genRadPoint(secondCenter.first, secondCenter.second, 0, radB, rgen));
        }
        std::random_shuffle(points.begin(), points.end());

        QString templStr1 = "|**x**|";
        QString templStr2 = "|**y**|";
        QString addStr1;
        QString addStr2;

        bool firstStep = true;

        for(const auto &el : qAsConst(points)){
            if(!firstStep){
                addStr1 += ",";
                addStr2 += ",";
            }
            else{
                firstStep = false;
            }
            const auto v1 = ConvertDoubleToStringWithAccuracy(el.first, 1);
            const auto v2 = ConvertDoubleToStringWithAccuracy(el.second, 1);
            templStr1 += v1 + "|";
            templStr2 += v2 + "|";
            addStr1 += v1;
            addStr2 += v2;
        }
        result.mainData.append(templStr1 + "\r\n" + templStr2);
        result.addData.append(addStr1);
        result.addData.append(addStr2);


        result.mainData.append(QString::number(clusterCount));

        int clC1x = genRangePoint(firstCenterA, 0, 8, rgen);
        int clC1y = genRangePoint(firstCenterB, 0, 5, rgen);

        int clC2x = genRangePoint(secondCenter.first, 0, 4, rgen);
        int clC2y = genRangePoint(secondCenter.second, 0, 7, rgen);

        auto str1 = QString::number(clC1x);
        auto str2 = QString::number(clC1y);
        auto str3 = QString::number(clC2x);
        auto str4 = QString::number(clC2y);
        templStr1 = QString("[(%1, %2), (%3, %4)]").arg(str1, str2,str3,str4);
        result.mainData.append(templStr1);
        result.addData.append(QString("%1,%2,%3,%4").arg(str1, str2,str3,str4));

        int addPointX = rgen.bounded(-30, 30);
        int addPointY = rgen.bounded(-30, 30);

        str1 = QString::number(addPointX);
        str2 = QString::number(addPointY);

        result.mainData.append(QString("(%1, %2)").arg(str1, str2));
        result.addData.append(QString("%1,%2").arg(str1, str2));

    }
    // задание 2
    {
        float firstCenterX = rgen.bounded(10, 20);
        float secondCenterX = genRangePoint(firstCenterX, 2, 7, rgen);

//        result.addData.append(QString("%1, %2").arg(firstCenterX).arg(secondCenterX));

        const float radA = /*rgen.generateDouble() **/ 7;
        const float radB = /*rgen.generateDouble() **/ 7;

        const float skoX1 = rgen.generateDouble() * 4 + 1;
        const float skoX2 = rgen.generateDouble() * 4 + 1;

//        const float skoX1 =  1;
//        const float skoX2 = 1;

        float newPoint = 0;
        newPoint = rgen.generateDouble() * std::abs(secondCenterX - firstCenterX) + std::min(firstCenterX, secondCenterX) + 0.25;
        float pc1 = rgen.generateDouble() * 0.4 + 0.3;
        float pc2 = 1 - pc1;
//        float pc1 = 0.5;
//        float pc2 = 0.5;

        QList<std::pair<float, std::pair<float, float>>> points;
        for(int i = 0; i < 5; ++i){
            const auto a = genRangePoint(firstCenterX, 0.5, radA, rgen);
            const auto b = genRangePoint(secondCenterX, 0.5, radB, rgen);

            points.append({a, PointPC(a, firstCenterX, secondCenterX, skoX1, skoX2, pc1, pc2)});
            points.append({b, PointPC(b, firstCenterX, secondCenterX, skoX1, skoX2, pc1, pc2)});
        }

        std::random_shuffle(points.begin(), points.end());

        QString templStr1;
        QString addStr1;
        QString addStr2;
        QString addStr3;

        bool firstStep = true;

        for(const auto &el : qAsConst(points)){
            if(!firstStep){
                addStr1 += ",";
                addStr2 += ",";
                addStr3 += ",";
                templStr1 += "\r\n";
            }
            else{
                firstStep = false;
            }
            const auto x = ConvertDoubleToStringWithAccuracy(el.first, 1);
            const auto v1 = ConvertDoubleToStringWithAccuracy(el.second.first , 1);
            const auto v2 = ConvertDoubleToStringWithAccuracy(el.second.second, 1);
            templStr1 += "|" + x + "|" + v1 + "|" + v2 + "|";
            addStr1 += x;
            addStr2 += v1;
            addStr3 += v2;
        }

        result.mainData.append(templStr1);
        result.addData.append(addStr1);
        result.addData.append(addStr2);
        result.addData.append(addStr3);


        result.mainData.append(ConvertDoubleToStringWithAccuracy(firstCenterX, 1));
        result.mainData.append(ConvertDoubleToStringWithAccuracy(secondCenterX, 1));
        result.mainData.append(ConvertDoubleToStringWithAccuracy(skoX1, 1));
        result.mainData.append(ConvertDoubleToStringWithAccuracy(skoX2, 1));

        result.mainData.append(ConvertDoubleToStringWithAccuracy(pc1, 1));
        result.mainData.append(ConvertDoubleToStringWithAccuracy(pc2, 1));
        result.mainData.append(ConvertDoubleToStringWithAccuracy(newPoint, 1));

    }
    // задание 3
    {
        QString templStr1 = "";
        QString addStr1;
        bool firstStep = true;
        for(int i = 0; i < 6; ++i){
            if(!firstStep){
                templStr1 += "\r\n";
            }
            else{
                firstStep = false;
            }
            templStr1 += QString("|**x<sub>%1</sub>**").arg(i);

            addStr1 = "";
            for(int j = 0; j < 5; ++j){
                if(j != 0){
                    addStr1 += ",";
                }
                const auto valStr = QString::number(rgen.generateDouble() < 0.5 ? 0 : 1);
                templStr1 += "|"+ valStr;
                addStr1 += valStr;
            }
            templStr1 += "|";

            result.addData.append(addStr1);
        }
        result.mainData.append(templStr1);
    }
    // задание 4
    {
        QList<QPointF> points;
        QPointF p;
        int maxX = 20;
        int maxY = 11;
        for(int i = 0; i < 25; ++i){
            double m = rgen.generateDouble();
            if(i == 0 || m < 0.35){
                do{
                    p = QPointF{(float)rgen.bounded(1, maxX), (float)rgen.bounded(1, maxY)};
                }while(points.contains(p));
                points.append(p);
//                qDebug() << "Add new" << p;
            }
            else if(m < 0.75){
                int ri = rgen.bounded(0, points.size());
                const auto &sPoint = points[ri];
                int j = 0;
                do{
                    p = QPointF{sPoint.x() + rgen.bounded(3)-1, sPoint.y() + rgen.bounded(3)-1};
                    ++j;
                }while((points.contains(p) || p.x()<= 0 || p.x() >= maxX || p.y() <= 0 || p.y() >= maxY) && j < 10);
                if(j == 10){
                    i--;
//                    qDebug() << "Try again after near";
                }
                else{
                    points.append(p);
//                    qDebug() << "Add near" << p;
                }
            }
            else{
                int ri = rgen.bounded(0, points.size());
                const auto &sPoint = points[ri];
                int j = 0;
                do{
                    p = QPointF{
                            sPoint.x() + (rgen.generateDouble() < 0.5 ? 1 : -1) * rgen.bounded(maxX/2, maxX),
                            sPoint.y() + (rgen.generateDouble() < 0.5 ? 1 : -1) * rgen.bounded(maxY/2, maxY)};
                    j++;
                }while((points.contains(p) || p.x()<= 0 || p.x() >= maxX || p.y() <= 0 || p.y() >= maxY) && j < 20);

                if(j == 20){
                    i--;
//                    qDebug() << "Try again after far";
                }
                else{
                    points.append(p);
//                    qDebug() << "Add far" << p;
                }
            }
        }

        QString addStr1;
        QString addStr2;
        bool first = true;
        for(const auto &el: qAsConst(points)){
            if(first){
                first = false;
            }
            else{
                addStr1 += ",";
                addStr2 += ",";
            }
            addStr1 += QString::number((int)el.x());
            addStr2 += QString::number((int)el.y());
        }
        result.addData.append(addStr1);
        result.addData.append(addStr2);

        const auto img = drawPoints(points, maxX, maxY, 700, 700.0 * maxY/maxX);
        QString imageName("n5_%1_1.png");
        imageName = imageName.arg(currentVariant + 1);
        result.mainData.append(imageName);
        result.images.insert(imageName, img);


        result.mainData.append(QString::number(rgen.bounded(2, 5)));
        result.mainData.append(QString::number(rgen.bounded(2, 6)));

        result.mainData.append(QString::number(rgen.bounded(3, 5)));
        result.mainData.append(QString::number(rgen.bounded(2, 7)));

        result.mainData.append(QString::number(rgen.bounded(1, 5)));
        result.mainData.append(QString::number(rgen.bounded(2, 7)));

        result.mainData.append(QString::number(rgen.bounded(1, 4)));
        result.mainData.append(QString::number(rgen.bounded(4, 11)));

        result.mainData.append(QString::number(rgen.bounded(2, 5)));
        result.mainData.append(QString::number(rgen.bounded(2, 6)));
    }
    return result;
}

GenReturnType WorkGenerator::genWork_6(int currentVariant)
{
    GenReturnType result;

    result.mainData.append(QString::number(currentVariant + 1));

    const auto rSeed = mGenSeed + currentVariant + 1;

    std::random_device rd{};
    std::default_random_engine generator;
    if(mIsRandomSeed){
        generator.seed(rd());
    }
    else{
        generator.seed(rSeed);
        std::srand(rSeed);
    }
    QRandomGenerator rgen(mIsRandomSeed ?
                          (QDateTime::currentMSecsSinceEpoch() / (currentVariant + 1)) :
                          rSeed);

    // задание 1
    {
        int pCount = 5;
        int firstCenterA = rgen.bounded(6, 15);
        float secondCenterA = genRangePoint(firstCenterA, 3, 6, rgen);

        struct P{
            QChar a1;
            QChar a2;
            float a3;
            QChar cl;
        };
        QList<P> points;

        int countA1[2] = {0,0};
        int countA2[3] = {0,0,0};


        const char startC = 'A';
        const auto endC = 'X';

        const char a1C1 = rgen.bounded(startC, endC+1);
        char a1C2 = 0;
        do{
            a1C2 = rgen.bounded(startC, endC+1);
        } while(a1C1 == a1C2);


        const char a2C1 = rgen.bounded(startC, endC+1);
        char a2C2 = 0, a2C3 = 0;
        do{
            a2C2 = rgen.bounded(startC, endC+1);
            a2C3 = rgen.bounded(startC, endC+1);
        } while(a2C1 == a2C2 || a2C1 == a2C3 || a2C2 == a2C3);

        const QList<QChar> a1V {a1C1, a1C2};
        const QList<QChar> a2V {a2C1, a2C2, a2C3};

        bool allInSet = false;
        for(int i = 0; i < pCount || !allInSet; ++i){
            const auto p1 = rgen.generateDouble();
            const auto p2 = rgen.generateDouble();

            const auto p1a1 = rgen.generateDouble() > 0.7 ? 0 : 1;
            const auto p1a2 = p1 < 0.3 ? 2 : (p1 < 0.6 ? 1 : 0);
            const auto p2a1 = rgen.generateDouble() > 0.75 ? 1 : 0;
            const auto p2a2 = p2 < 0.2 ? 1 : (p2 < 0.4 ? 0 : 2);

            countA1[p1a1]++;
            countA1[p2a1]++;
            countA2[p1a2]++;
            countA2[p2a2]++;

            points.append({a1V[p1a1], a2V[p1a2],
                          genRangePoint(firstCenterA, 0, 3, rgen),
                          'Y'});
            points.append({a1V[p2a1], a2V[p2a2],
                          genRangePoint(secondCenterA, 0, 3, rgen),
                          'N'});

            allInSet = countA1[0] && countA1[1] && countA2[0] && countA2[1] && countA2[2];
        }

        std::random_shuffle(points.begin(), points.end());

        QString templ = "";
        for(auto i : range(points.size())){
            if(i != 0){
                templ += "\r\n";
            }
            const auto &p = points[i];
            templ += QString("|*x<sub>%1</sub>*|%2|%3|%4|%5|")
                    .arg(i)
                    .arg(p.a1)
                    .arg(p.a2)
                    .arg(ConvertDoubleToStringWithAccuracy(p.a3, 1))
                    .arg(p.cl);
        }
        result.mainData.append(templ);

        const auto pv1 = rgen.generateDouble();
        const auto pv2 = rgen.generateDouble();
        const auto pv3 = rgen.generateDouble();
        const auto p1a1 = rgen.generateDouble() > 0.7 ? 0 : 1;
        const auto p1a2 = pv1 < 0.3 ? 2 : (pv1 < 0.6 ? 1 : 0);

        const auto p2a1 = rgen.generateDouble() > 0.75 ? 1 : 0;
        const auto p2a2 = pv2 < 0.2 ? 1 : (pv2 < 0.4 ? 0 : 2);

        const auto p3a1 = rgen.generateDouble() > 0.5 ? 1 : 0;
        const auto p3a2 = pv3 < 0.33 ? 1 : (pv3 < 0.33 ? 0 : 2);


        P p1 {a1V[p1a1], a2V[p1a2],
            genRangePoint(firstCenterA, 0, 3, rgen),
            'Y'};
        P p2 {a1V[p2a1], a2V[p2a2],
            genRangePoint(secondCenterA, 0, 3, rgen),
            'N'};

        P p3 {a1V[p3a1], a2V[p3a2],
            genRangePoint((secondCenterA+firstCenterA)/2, 0, 10, rgen),
            'N'};

        const auto pointsStr = QString("(%1),\r\n(%2),\r\n(%3)");

        const auto pstr1 = QString("%1,%2,%3").arg(p1.a1).arg(p1.a2).arg(ConvertDoubleToStringWithAccuracy(p1.a3, 1));
        const auto pstr2 = QString("%1,%2,%3").arg(p2.a1).arg(p2.a2).arg(ConvertDoubleToStringWithAccuracy(p2.a3, 1));
        const auto pstr3 = QString("%1,%2,%3").arg(p3.a1).arg(p3.a2).arg(ConvertDoubleToStringWithAccuracy(p3.a3, 1));

        result.mainData.append(pointsStr.arg(pstr1, pstr2, pstr3));

        QString add1, add2, add3, add4;
        for(auto i : range(points.size())){
            if(i != 0){
                add1 += ",";
                add2 += ",";
                add3 += ",";
                add4 += ",";
            }
            const auto &p = points[i];
            add1.append(p.a1);
            add2.append(p.a2);
            add3.append(ConvertDoubleToStringWithAccuracy(p.a3, 1));
            add4.append(p.cl);
        }
        result.addData.append(add1);
        result.addData.append(add2);
        result.addData.append(add3);
        result.addData.append(add4);

        result.addData.append(pstr1);
        result.addData.append(pstr2);
        result.addData.append(pstr3);

    }
    // задание 2
    {
        int pCount = 5;

        int firstCenterX = rgen.generateDouble() * 5;
        int firstCenterY = rgen.generateDouble() * 5;
        auto secondCenterXY = genRadPoint(firstCenterX, firstCenterY, 6, 10, rgen);

        float radA = rgen.generateDouble() * 3.5 + 1;
        float radB = rgen.generateDouble() * 2 + 1;

        struct P{
            float x;
            float y;
            int cl;
        };

        QList<P> points;
        for(int i = 0; i < pCount; ++i){
            auto p1 = genRadPoint(firstCenterX, firstCenterY, 0, radA, rgen);
            auto p2 = genRadPoint(secondCenterXY.first, secondCenterXY.second, 0, radB, rgen);

            points.append({p1.first, p1.second, 1});
            points.append({p2.first, p2.second, -1});
        }
        std::random_shuffle(points.begin(), points.end());

        QString templ = "";
        for(auto i : range(points.size())){
            if(i != 0){
                templ += "\r\n";
            }
            const auto &p = points[i];
            templ += QString("|%1|(%2, %3)|%4|")
                    .arg(i)
                    .arg(ConvertDoubleToStringWithAccuracy(p.x, 1),
                         ConvertDoubleToStringWithAccuracy(p.y, 1))
                    .arg(p.cl);
        }
        result.mainData.append(templ);

        const auto addPointXY = genRadPoint((firstCenterX + secondCenterXY.first)/2,
                                           (firstCenterY + secondCenterXY.second), 1, 5, rgen);


        result.mainData.append(QString("(%1, %2)").arg(ConvertDoubleToStringWithAccuracy(addPointXY.first, 1),
                                                   ConvertDoubleToStringWithAccuracy(addPointXY.second, 1)));

        QString add1, add2, add3;
        for(auto i : range(points.size())){
            if(i != 0){
                add1 += ",";
                add2 += ",";
                add3 += ",";
            }
            const auto &p = points[i];
            add1.append(ConvertDoubleToStringWithAccuracy(p.x, 1));
            add2.append(ConvertDoubleToStringWithAccuracy(p.y, 1));
            add3.append(QString::number(p.cl));
        }
        result.addData.append(add1);
        result.addData.append(add2);
        result.addData.append(add3);

        result.addData.append(QString("%1,%2").arg(ConvertDoubleToStringWithAccuracy(addPointXY.first, 1),
                                                   ConvertDoubleToStringWithAccuracy(addPointXY.second, 1)));

    }
    return result;
}

GenReturnType WorkGenerator::genWork_3_4(int currentVariant)
{
    QStringList templateData;
    QStringList addData;
    QHash<QString, QImage> images;

    templateData.append(QString::number(currentVariant + 1));

    const auto rSeed = mGenSeed + currentVariant + 1;

    std::random_device rd{};
    std::default_random_engine generator;
    if(mIsRandomSeed){
        generator.seed(rd());
    }
    else{
        generator.seed(rSeed);
        std::srand(rSeed);
    }
    QRandomGenerator rgen(mIsRandomSeed ?
                          (QDateTime::currentMSecsSinceEpoch() / (currentVariant + 1)) :
                          rSeed);

    // задание 1
    {
        const int MaxCountL = 7;
        const char StartL {'A'};
        for(int i = 0; i < 10; i++){
            QString str;
            do{
                str.clear();
                for(int j = 0; j < MaxCountL; ++j){
                    if(rgen.generateDouble() < 0.45){
                        str += char((int)StartL + j);
                    }
                }
            }
            while(str.length() <= 1);
            templateData.append(str);
        }

        const int apLevel0 = rgen.bounded(3, 6);

        templateData.append(QString::number(apLevel0));
    }

    // задание 2
    {
        const int aElCount[] = {0, 2, 3};
        auto g = new LinkedGraph;


        int generalElCount = 0;
        do{
            g->clear();
            generalElCount = 0;

            const auto l1Count = 4 /*rgen.bounded(3, 5)*/;
            generalElCount += l1Count;

            bool hasOneEl = false;

            for(int i = 0; i < l1Count; ++i){
                const auto n_el1 = g->addChildElement(0);
                auto l2Count = aElCount[rgen.bounded(0, 3)];

                if(l2Count == 0)
                    hasOneEl = true;

                if(i == (l1Count-1) && !hasOneEl){
                    l2Count = 0;
                    hasOneEl = true;
                }

                generalElCount += l2Count;

                for(int j = 0; j < l2Count; ++j){
                    const auto n_el2 = n_el1->addChildElement(0);
                    const auto l3Count = aElCount[rgen.bounded(0, 3)];
                    generalElCount += l3Count;

                    for(int k = 0; k < l3Count; ++k){
                        n_el2->addChildElement(0);
                    }
                }
            }
            auto gList = g->espDetour();

            char elN = 'A';
            for(const auto &el : qAsConst(gList.first)){
                el->setValue(QString(elN));
                elN++;
            }
        }
        while(generalElCount < 13 || generalElCount > 18);

        const auto img = g->drawGraph();
        QString imageName("n3_%1_1.png");
        imageName = imageName.arg(currentVariant + 1);

        QVector<QString> AllowedSet;
        const auto l = g->espDetour();

        for(int i = 0; i < l.second; ++i){
            AllowedSet.append(l.first[i]->getValue().toString());
        }

        addData.append(QString::number(l.second));
        QString addStr;
        for(int i = 0; i < l.first.size(); ++i){
            addStr += l.first[i]->getValue().toString();
        }
        addData.append(addStr);
        addData.append(g->toJSONTree());


        for(int i = 0; i < 10; i++){
            QString str;
            int sCount = 0;
            do{
                sCount = 0;
                str.clear();
                for(int j = 0; j < AllowedSet.length(); ++j){
                    if(rgen.generateDouble() < 0.4){
    //                    str += (str.length() > 0 ? ", " : "") + QString::number(AllowedSet[j]);
                        str += AllowedSet[j];
                        sCount++;
                    }
                }
            }
            while(sCount <= 2);
            templateData.append(str);
        }

        const int apLevel3 = rgen.bounded(3, 8);
        templateData.append(QString::number(apLevel3));


        templateData.append(imageName);

        images.insert(imageName, img);
    }

    // задание 3
    {
        const int MaxCountL = 5;
        const char StartL {'A'};
        for(int i = 0; i < 8; i++){
            QString str;
            do{
                str.clear();
                for(int j = 0; j < MaxCountL; ++j){
                    if(rgen.generateDouble() < 0.6){
                        str += char((int)StartL + j);
                    }
                }
            }
            while(str.length() <= 1);
            templateData.append(str);
        }

        const int apLevel0 = rgen.bounded(1, 6);
        templateData.append(QString::number(apLevel0));

        QString str;
        do{
            str.clear();
            for(int j = 0; j < MaxCountL; ++j){
                if(rgen.generateDouble() < 0.4){
                    str += char((int)StartL + j);
                }
            }
        }
        while(str.length() == 0);
        templateData.append(str);

    }

    return {templateData, addData, images};
}

GenReturnType WorkGenerator::genExampleWork_4(int)
{
    GenReturnType result;
    result.mainData.append("0");
    // задание 1
    result.mainData.append("B");
    result.mainData.append("D");
    result.mainData.append("BC");
    result.mainData.append("C");
    result.mainData.append("ABCD");
    result.mainData.append("AB");
    result.mainData.append("ABC");
    result.mainData.append("AB");
    result.mainData.append("3");
    result.mainData.append("ABC");

    // задание 2
    {
    QList<char> ABCSymbolList = {'A', 'B', 'C'};

    const auto fullABCSet = GenFullSet(ABCSymbolList);

    QStringList resultSet2 = {"A", "B", "ABC", "BC", "AC"};
    QString resultStr;
    for(const auto &el : resultSet2){
        resultStr += el + ";";
    }
    result.addData.append(resultStr.chopped(1));

    QVector<QStringList> listByLevels(ABCSymbolList.size());
    for(const auto &el : qAsConst(fullABCSet)){
        listByLevels[el.length()-1].append(el);
    }

    LinkedGraph g;
    auto root = g.setRootElement("Ø");

    const static QString TreeValueTemplate("%1(%2)");

    for(int k = 0; k < listByLevels.size(); ++k){
        if(k == 0){
            for(const auto &el : qAsConst(listByLevels[k])){
                root->addChildElement(el);
            }
        }
        else{
            const auto k_1LevelElements = g.getElementsByLevel(k);
            for(const auto &el : qAsConst(listByLevels[k])){


                LinkedElement *currentElement = nullptr;
                for(const auto &listEl : k_1LevelElements){
                    if(el.startsWith(listEl->getValue().toString())){
                        currentElement = listEl->addChildElement(el);
                    }
                    else if(IsSubSequence(listEl->getValue().toString(), el)){
                        if(currentElement)
                            currentElement->connectParent(listEl);
                    }
                }

            }

        }
    }

    QString fqStr;
    g.foreachDetour([=, &fqStr](auto *el){
        if(el->getLevel() != 0){
            const auto v = el->getValue().toString();
            const auto fq = GetFreqSubstringInSet(v, resultSet2);
            el->setValue(TreeValueTemplate.arg(v).arg(fq));
            fqStr += QString("%1:%2;").arg(v).arg(fq);
        }
        return true;
    });
    result.addData.append(fqStr.chopped(1));


    const auto img = g.drawGraph(962, 481);
    QString imageName("n4_0_1.png");
    result.mainData.append(imageName);
    result.images.insert(imageName, img);

    result.mainData.append("2");
    result.mainData.append("BC");
    } // 2

    // задание 3
    result.mainData.append("ATATA");
    result.mainData.append("TTATG");
    result.mainData.append("CAT");
    result.mainData.append("ACT");
    result.mainData.append("GGAATT");
    result.mainData.append("3");
    result.mainData.append("TAG");

    return result;
}

GenReturnType WorkGenerator::genExampleWork_5(int)
{
    GenReturnType result;
    result.mainData.append("0");
    // задание 1
    {
        result.mainData.append("|**x**|1|2|3|4|1|2|4|3| | |\r\n"
                               "|**y**|1|2|3|4|2|1|3|4| | |");

        result.mainData.append("2");
        result.mainData.append("[(0,0), (4,4)]");
        result.mainData.append("(2.5,1.5)");

        result.addData.append("1,1,1");
        result.addData.append("1,1,1");
        result.addData.append("1,2,3,4,1,2,4,3");
        result.addData.append("1,2,3,4,2,1,3,4");
        result.addData.append("0,0,4,4");
        result.addData.append("2.5,1.5");
    }
    // задание 2
    {
        QString d =
                "|2|0.9|0.1|\r\n"
                "|3|0.8|0.2|\r\n"
                "|7|0.3|0.7|\r\n"
                "|9|0.1|0.9|\r\n"
                "|2|0.9|0.1|\r\n"
                "|1|0.8|0.2|";
        result.mainData.append(d);

        result.addData.append("2,3,7,9,2,1");
        result.addData.append("0.9,0.8,0.3,0.1,0.9,0.8");
        result.addData.append("0.1,0.2,0.7,0.9,0.1,0.2");

        result.mainData.append("2");
        result.mainData.append("7");
        result.mainData.append("1");
        result.mainData.append("1");
        result.mainData.append("0.5");
        result.mainData.append("0.5");
        result.mainData.append("5");
    }
    // задание 3
    {
        QString d =
                "|**x<sub>0</sub>**|1|0|1|1|0|\r\n"
                "|**x<sub>1</sub>**|1|1|0|1|0|\r\n"
                "|**x<sub>2</sub>**|0|0|1|1|0|\r\n"
                "|**x<sub>3</sub>**|0|1|0|1|0|\r\n"
                "|**x<sub>4</sub>**|1|0|1|0|1|\r\n"
                "|**x<sub>5</sub>**|0|1|1|0|0|";
        result.mainData.append(d);

        d =
            "1,0,1,1,0\r\n"
            "1,1,0,1,0\r\n"
            "0,0,1,1,0\r\n"
            "0,1,0,1,0\r\n"
            "1,0,1,0,1\r\n"
            "0,1,1,0,0";
        result.addData.append(d);
    }
    // задание 4
    {
        QList<QPointF> points = {{5,8}, {6,7}, {5,6}, {6,5}, {2,4}, {15,5}, {15,4}, {14,6}, {3,4}, {7,4}, {3,3}, {13,6}, {13,7}, {8,2}};
        int maxX = 20;
        int maxY = 11;


        QString addStr1;
        QString addStr2;
        bool first = true;
        for(const auto &el: qAsConst(points)){
            if(first){
                first = false;
            }
            else{
                addStr1 += ",";
                addStr2 += ",";
            }
            addStr1 += QString::number((int)el.x());
            addStr2 += QString::number((int)el.y());
        }
        result.addData.append(addStr1);
        result.addData.append(addStr2);

        const auto img = drawPoints(points, maxX, maxY, 700, 700.0 * maxY/maxX);
        QString imageName("n5_%1_1.png");
        imageName = imageName.arg(0);
        result.mainData.append(imageName);
        result.images.insert(imageName, img);

        result.mainData.append("2");
        result.mainData.append("3");

        result.mainData.append("4");
        result.mainData.append("3");

        result.mainData.append("2");
        result.mainData.append("4");

        result.mainData.append("1");
        result.mainData.append("8");

        result.mainData.append("4");
        result.mainData.append("4");
    }

    return result;
}

GenReturnType WorkGenerator::genExampleWork_6(int)
{
    GenReturnType result;

    result.addData.append("T,T,T,F,F,F,F,T");
    result.addData.append("A,T,G,A,T,G,A,T");
    result.addData.append("5,7,8,3,7,4,5,6");
    result.addData.append("Y,Y,N,Y,N,N,N,Y");

    result.addData.append("T,T,4");
    result.addData.append("F,A,7");
    result.addData.append("F,G,11");

    result.addData.append("4.1,2.5,2.0,2.2,1.4,3.9,3.0,1.0");
    result.addData.append("3.0,4.2,1.2,2.0,1.0,4.1,3.2,2.1");
    result.addData.append("1,1,-1,-1,-1,1,1,-1");

    result.addData.append("1,4");


    result.mainData.append("0");

    result.mainData.append( "|*x<sub>0</sub>*|T|A|5|Y|\r\n"
                            "|*x<sub>1</sub>*|T|T|7|Y|\r\n"
                            "|*x<sub>2</sub>*|T|G|8|N|\r\n"
                            "|*x<sub>3</sub>*|F|A|3|Y|\r\n"
                            "|*x<sub>4</sub>*|F|T|7|N|\r\n"
                            "|*x<sub>5</sub>*|F|G|4|N|\r\n"
                            "|*x<sub>6</sub>*|F|A|5|N|\r\n"
                            "|*x<sub>7</sub>*|T|T|6|Y|");

    result.mainData.append("(T,T,4),\r\n(F,A,7),\r\n(F,G,11)");

    result.mainData.append( "|0|(4.1, 3.0)|1|\r\n"
                            "|1|(2.5, 4.2)|1|\r\n"
                            "|2|(2.0, 1.2)|-1|\r\n"
                            "|3|(2.2, 2.0)|-1|\r\n"
                            "|4|(1.4, 1.0)|-1|\r\n"
                            "|5|(3.9, 4.1)|1|\r\n"
                            "|6|(3.0, 3.2)|1|\r\n"
                            "|7|(1.0, 2.1)|-1|");

    result.mainData.append("(1, 4)");


    return result;
}

GenReturnType WorkGenerator::genExampleWork_3_4(int)
{
    const auto rSeed = mGenSeed;

    std::random_device rd{};
    std::default_random_engine generator;
    if(mIsRandomSeed){
        generator.seed(rd());
    }
    else{
        generator.seed(rSeed);
        std::srand(rSeed);
    }
    QRandomGenerator rgen(mIsRandomSeed ?
                          (QDateTime::currentMSecsSinceEpoch() / 88) :
                          rSeed);

    GenReturnType result;
    result.mainData.append("0");

    // задание 1
    {
        result.mainData.append("B");
        result.mainData.append("D");
        result.mainData.append("A");
        result.mainData.append("C");
        result.mainData.append("ABCD");
        result.mainData.append("AB");
        result.mainData.append("ABC");
        result.mainData.append("AC");
        result.mainData.append("AD");
        result.mainData.append("BC");
        result.mainData.append("5");
    }

    // задание 2
    {
//        const int aElCount[] = {0, 2, 3};
        auto g = new LinkedGraph;

        int generalElCount = 0;
        do{
            g->clear();
            generalElCount = 0;

            const auto l1Count = 2 /*rgen.bounded(3, 5)*/;
            generalElCount += l1Count;

            bool hasOneEl = false;

            for(int i = 0; i < l1Count; ++i){
                const auto n_el1 = g->addChildElement(0);
                auto l2Count = 2;

                if(l2Count == 0)
                    hasOneEl = true;

                if(i == (l1Count-1) && !hasOneEl){
                    l2Count = 0;
                    hasOneEl = true;
                }

                generalElCount += l2Count;

                for(int j = 0; j < l2Count; ++j){
                    const auto n_el2 = n_el1->addChildElement(0);
                    const auto l3Count = 2;
                    generalElCount += l3Count;

                    for(int k = 0; k < l3Count; ++k){
                        n_el2->addChildElement(0);
                    }
                }
            }
            auto gList = g->espDetour();

            char elN = 'A';
            for(const auto &el : qAsConst(gList.first)){
                el->setValue(QString(elN));
                elN++;
            }
        }
        while(false);

        const auto img = g->drawGraph();
        QString imageName("n3_%1_1.png");
        imageName = imageName.arg(0);

        QVector<QString> AllowedSet;
        const auto l = g->espDetour();

        for(int i = 0; i < l.second; ++i){
            AllowedSet.append(l.first[i]->getValue().toString());
        }

        result.addData.append(QString::number(l.second));
        QString addStr;
        for(int i = 0; i < l.first.size(); ++i){
            addStr += l.first[i]->getValue().toString();
        }
        result.addData.append(addStr);
        result.addData.append(g->toJSONTree());


        for(int i = 0; i < 10; i++){
            QString str;
            int sCount = 0;
            do{
                sCount = 0;
                str.clear();
                for(int j = 0; j < AllowedSet.length(); ++j){
                    if(rgen.generateDouble() < 0.3){
    //                    str += (str.length() > 0 ? ", " : "") + QString::number(AllowedSet[j]);
                        str += AllowedSet[j];
                        sCount++;
                    }
                }
            }
            while(sCount <= 2);
            result.mainData.append(str);
        }

        const int apLevel3 = 4;
        result.mainData.append(QString::number(apLevel3));


        result.mainData.append(imageName);

        result.images.insert(imageName, img);
    }

    // задание 3
    {
        result.mainData.append("B");
        result.mainData.append("D");
        result.mainData.append("BC");
        result.mainData.append("C");
        result.mainData.append("ABCD");
        result.mainData.append("AB");
        result.mainData.append("ABC");
        result.mainData.append("AB");
        result.mainData.append("3");
        result.mainData.append("ABC");
    }

    return result;
}
