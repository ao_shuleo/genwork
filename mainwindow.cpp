#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "workgenerator.h"

#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mGen = new WorkGenerator(this);

    const auto aData = mGen->getAvailableWorks();
    for(const auto &el : qAsConst(aData)){
        ui->comboBox->addItem(el.first, el.second);
    }

    mGen->readSettings();
    setGUIFromSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setGUIFromSettings()
{
    ui->lineEdit->setText(mGen->mTemplatePath);
    ui->lineEdit_2->setText(mGen->mSaveDir);
    ui->lineEdit_3->setText(mGen->mFileNameTemplate);
    ui->spinBox->setValue(mGen->mVarCount);
    ui->comboBox->setCurrentIndex(mGen->mWorkNumber);
    ui->spinBox_2->setValue(mGen->mGenSeed);
    ui->checkBox->setChecked(mGen->mIsRandomSeed);

    ui->spinBox_2->setEnabled(!ui->checkBox->isChecked());

    ui->lineEdit_4->setText(mGen->mCopyFilesPath);
    ui->checkBox_2->setChecked(mGen->addCopyFiles);
    ui->widget_3->setEnabled(mGen->addCopyFiles);

    ui->checkBox_3->setChecked(mGen->mCreateDirForVar);
}

// генерировать
void MainWindow::on_pushButton_3_clicked()
{
    mGen->setTemplatePath(ui->lineEdit->text());
    mGen->setSavePath(ui->lineEdit_2->text());
    mGen->setVariantCount(ui->spinBox->value());
    mGen->setWorkNumber(ui->comboBox->currentData().toInt());
    mGen->setFileNameTemplate(ui->lineEdit_3->text());
    mGen->setGenerationSeed(ui->spinBox_2->value());
    mGen->setRandomGen(ui->checkBox->isChecked());

    mGen->setAddCopyFiles(ui->checkBox_2->isChecked());
    mGen->setCopyFilesPath(ui->lineEdit_4->text());

    mGen->setCreateDirForVar(ui->checkBox_3->isChecked());

    if(mGen->generate()){
        QMessageBox::information(this, tr("Информация"),
                                 tr("Файлы заданий успешно созданы"));
    }
    else{
        QMessageBox::critical(this, tr("Ошибка"),
                              tr("Ошибка генерации файлов заданий: %1").arg(mGen->getLastError()));
    }
}

// Путь к шаблону:
void MainWindow::on_pushButton_clicked()
{
    const auto fName = QFileDialog::getOpenFileName(nullptr, tr("Путь к файлу шаблона"), mGen->mTemplatePath);
    if(!fName.isEmpty()){
        ui->lineEdit->setText(fName);
        mGen->setTemplatePath(fName);
    }
}

// Путь генерации:
void MainWindow::on_pushButton_2_clicked()
{
    const auto dName = QFileDialog::getExistingDirectory(nullptr, tr("Путь к директории для генерации файлов"), mGen->mSaveDir);
    if(!dName.isEmpty()){
        ui->lineEdit_2->setText(dName);
        mGen->setSavePath(dName);
    }
}

void MainWindow::on_checkBox_stateChanged(int /*arg1*/)
{
    mGen->setRandomGen(ui->checkBox->isChecked());
    ui->spinBox_2->setEnabled(!ui->checkBox->isChecked());
}

void MainWindow::on_checkBox_2_stateChanged(int /*arg1*/)
{
    mGen->setAddCopyFiles(ui->checkBox_2->isChecked());
    ui->widget_3->setEnabled(ui->checkBox_2->isChecked());
}

// путь к директории-источника копирования файлов
void MainWindow::on_pushButton_4_clicked()
{
    const auto dName = QFileDialog::getExistingDirectory(nullptr, tr("Путь к директории доп. файлов для копирования"));
    if(!dName.isEmpty()){
        ui->lineEdit_4->setText(dName);
    }
}

// создавать дир. для каждого вар
void MainWindow::on_checkBox_3_stateChanged(int /*arg1*/)
{
    mGen->setCreateDirForVar(ui->checkBox_3->isChecked());
}

// генерировать пример варианта
void MainWindow::on_pushButton_5_clicked()
{
    mGen->setTemplatePath(ui->lineEdit->text());
    mGen->setSavePath(ui->lineEdit_2->text());
    mGen->setVariantCount(ui->spinBox->value());
    mGen->setWorkNumber(ui->comboBox->currentData().toInt());
    mGen->setFileNameTemplate(ui->lineEdit_3->text());
    mGen->setGenerationSeed(ui->spinBox_2->value());
    mGen->setRandomGen(ui->checkBox->isChecked());

    mGen->setAddCopyFiles(ui->checkBox_2->isChecked());
    mGen->setCopyFilesPath(ui->lineEdit_4->text());

    mGen->setCreateDirForVar(ui->checkBox_3->isChecked());

    if(mGen->generate(true)){
        QMessageBox::information(this, tr("Информация"),
                                 tr("Файл примера задания успешно создан"));
    }
    else{
        QMessageBox::critical(this, tr("Ошибка"),
                              tr("Ошибка генерации примера задания: %1").arg(mGen->getLastError()));
    }
}
