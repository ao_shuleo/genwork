#include "mainwindow.h"

#include <QApplication>
#include <QDebug>

#include "linkedgraph.h"

#include "utils.h"

void CreateSaveExampleTree(){
    LinkedGraph g;
    g.addChildElement("a");
    auto ch = g.addChildElement("f");
    ch->addChildElement("b");
    ch->addChildElement("c");
    ch = ch->addChildElement("g");
    ch->addChildElement("d");
    ch->addChildElement("e");

    auto v = g.espDetour();
    qDebug() << v.second;
    for(const auto &el : qAsConst(v.first))
        qDebug() << el->getValue().toString();
    qDebug() << g.toJSONTree();
    g.drawGraph().save("n3_0_1.png", "PNG");
}

void CreateRef4Graph(){
    LinkedGraph g;
    auto root = g.setRootElement("Ø(6)");
//    auto root = g.setRootElement({});
    auto ch = root->addChildElement("A(6)");
    auto ch2 = root->addChildElement("B(5)");
    auto ch3 = root->addChildElement("C(4)");
    auto ch4 = root->addChildElement("D(3)");

    auto ch11 = ch->addChildElement("AB(5)");
    auto ch12 = ch->addChildElement("AC(4)");
    auto ch13 = ch->addChildElement("AD(3)");
    auto ch14 = ch2->addChildElement("BC(3)");
    auto ch15 = ch2->addChildElement("BD(2)");
    auto ch16 = ch3->addChildElement("CD(2)");

    ch11->connectParent(ch2);
    ch12->connectParent(ch3);
    ch13->connectParent(ch4);
    ch14->connectParent(ch3);
    ch15->connectParent(ch4);
    ch16->connectParent(ch4);

    auto ch21 = ch11->addChildElement("ABC(3)");
    auto ch22 = ch11->addChildElement("ABD(2)");
    auto ch23 = ch12->addChildElement("ACD(2)");
    auto ch24 = ch14->addChildElement("BCD(1)");

    ch21->connectParent(ch12);
    ch21->connectParent(ch14);

    ch22->connectParent(ch13);
    ch22->connectParent(ch15);

    ch23->connectParent(ch13);
    ch23->connectParent(ch16);

    ch24->connectParent(ch15);
    ch24->connectParent(ch16);

    auto ch31 = ch21->addChildElement("ABCD(1)");
    ch31->connectParent(ch22);
    ch31->connectParent(ch23);
    ch31->connectParent(ch24);

    qDebug() << g.toJSONTree();
    qDebug() << g.toJSONGraph();
    g.drawGraph().save("ntest.png", "PNG");
}


void CreateTest2Graph(){
    QList<char> ABCSymbolList = {'A', 'B', 'C', 'D'};

    const auto fullABCSet = GenFullSet(ABCSymbolList);

    QStringList resultSet2 = {"D", "ABC", "BC", "ACD"};

    QVector<QStringList> listByLevels(ABCSymbolList.size());
    for(const auto &el : qAsConst(fullABCSet)){
        listByLevels[el.length()-1].append(el);
    }

    LinkedGraph g;
    auto root = g.setRootElement("Ø");

    const static QString TreeValueTemplate("%1(%2)");

    for(int k = 0; k < listByLevels.size(); ++k){
        if(k == 0){
            for(const auto &el : qAsConst(listByLevels[k])){
                root->addChildElement(el);
            }
        }
        else{
            const auto k_1LevelElements = g.getElementsByLevel(k);
            for(const auto &el : qAsConst(listByLevels[k])){


                LinkedElement *currentElement = nullptr;
                for(const auto &listEl : k_1LevelElements){
                    if(el.startsWith(listEl->getValue().toString())){
                        currentElement = listEl->addChildElement(el);
                    }
                    else if(IsSubSequence(listEl->getValue().toString(), el)){
                        if(currentElement)
                            currentElement->connectParent(listEl);
                    }
                }

            }

        }
    }

    QString fqStr;
    g.foreachDetour([=, &fqStr](auto *el){
        if(el->getLevel() != 0){
            const auto v = el->getValue().toString();
            const auto fq = GetFreqSubstringInSet(v, resultSet2);
            el->setValue(TreeValueTemplate.arg(v).arg(fq));
            fqStr += QString("%1:%2;").arg(v).arg(fq);
        }
        return true;
    });


    g.drawGraph(962, 481).save("test2.png");
}



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    CreateSaveExampleTree();
//    CreateRef4Graph();
//    CreateTest2Graph();

    MainWindow w;
    w.show();
    return a.exec();
}
