#ifndef MRANGE_H
#define MRANGE_H

namespace mrange {

template<typename D>
class range_interator{
    D i {0};
    const D mStep {1};
public:
    explicit range_interator(D pos = 0, D step = 1) : i(pos), mStep(step) {}
    const D &operator*() const { return i; }
    range_interator &operator++() { i += mStep; return *this; }
    bool operator != (const range_interator &other) const { return mStep > 0 ? i < other.i : i > other.i; }
};

template<typename D>
class range{
    const D mFrom {0}, mTo {0};
    D mStep {1};
public:
    explicit range(D to) : mTo(to) {
        if(mFrom > mTo) mStep = -mStep;
    }
    explicit range(D from, D to, D step = 1) : mFrom(from), mTo(to), mStep(step) {
        if((mStep > 0 && mFrom > mTo) || (mStep < 0 && mFrom < mTo)) mStep = -mStep;
    }
    range_interator<D> begin() const { return range_interator<D>(mFrom, mStep); }
    range_interator<D> end() const { return range_interator<D>(mTo, mStep); }
};
}

mrange::range<int> range(int from, int to, int step = 1) { return mrange::range<int>(from, to, step); }
mrange::range<int> range(int to) { return mrange::range<int>(to); }

mrange::range<float> frange(float from, float to, float step = 1) { return mrange::range<float>(from, to, step); }
mrange::range<float> frange(float to) { return mrange::range<float>(to); }


#endif // MRANGE_H
