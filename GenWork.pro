QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    linkedgraph.cpp \
    main.cpp \
    mainwindow.cpp \
    qcustomplot.cpp \
    utils.cpp \
    workgenerator.cpp

HEADERS += \
    linkedgraph.h \
    mainwindow.h \
    mrange.h \
    qcustomplot.h \
    utils.h \
    workgenerator.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
