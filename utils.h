#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QRandomGenerator>
#include <QImage>

QImage drawPoints(const QList<QPointF> &mPoints, int maxX, int maxY, int width = 700, int height = 525);
double NormalPDF(double x, double m, double s);
double NormalCDF(double x, double m, double s);
QString ConvertDoubleToStringWithAccuracy(const double val, const int accuracy);
std::pair<float, float> genRadPoint(float x, float y, float rMin, float rMax, QRandomGenerator &rgen);
float genRangePoint(float x, float rMin, float rMax, QRandomGenerator &rgen);
QStringList _GenFullSet(int level, const QList<char> &I, const QString &prefix);
QStringList GenFullSet(const QList<char> &I);
bool r_IsSubSequence(const QString &str1, const QString &str2, int m, int n);
bool IsSubSequence(const QString &str1, const QString &str2);
int GetFreqSubstringInSet(const QString &sub, const QStringList &dataSet);

#endif // UTILS_H
